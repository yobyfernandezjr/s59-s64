const User = require('../models/User');
const Product = require('../models/Product');
const bcrypt = require('bcrypt');
const auth = require('../auth');

//Check if the email already exists
module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {	
		if (result.length > 0) {
			return true
		} else {
			return false
		}
	})
};

// User registration
module.exports.registerUser = (reqBody) => {	
	let newUser = new User({
		fullName: reqBody.fullName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10) 
	})	
	return newUser.save().then((user, error) => {
		if(error) {
			return false
		} else {
			return true
		}
	})
};

// User login
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if (result == null) {
			return false
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if(isPasswordCorrect) {
				return { access: auth.createAccessToken(result)}
			} else {
				return false
			}
		}
	})
};

// All Users details
module.exports.getProfile = (userData) => {
	return User.findById(userData.userId).then(result => {
		console.log(userData)
		if (result == null) {
			return false
		} else {
		console.log(result)
		result.password = "*****";
		return result;
		}
	});
};

module.exports.reserve = async (data) => {
	if (data.isAdmin == true) {
		return false
	} else {
		let isUserUpdated = await User.findById(data.userId).then(user => {
			user.reservation.push({productId: data.productId});
			return user.save().then((user, error) => {
				if(error) {
					return false
				} else {
					return true
				}
			})
		})

		let isProductUpdated = await Product.findById(data.productId).then(product => {
			product.reservee.push({userId: data.userId});
			return product.save().then((product, error) => {
				if (error) {
					return false
				} else {
					return true
				}
			})
		})

		if (isUserUpdated && isProductUpdated) {
			return true
		} else {
			return false
		}
	}
};

// Set user to admin
// async function setUserAdmin (req, res, next) {
// 	const user = auth.decode(req.headers.authorization);
// 	if (user.isAdmin) {
// 		try {
// 			result = await User.findByIdAndUpdate(req.body.userId, {isAdmin: req.body.isAdmin});
// 			return res.send('Change status priviledge successful.');
// 		} catch (error) {
// 			next (error);
// 		};
// 	} else {
// 		return res.send('Not authorized to change status.')
// 	};
// };
	
// module.exports = {
// 	setUserAdmin: setUserAdmin
// };